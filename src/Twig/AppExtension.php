<?php


namespace App\Twig;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('interpret', [$this, 'interpretArticle']),
        ];
    }

  #fonction callback qui s'execute quand # ou @ est appeler.

    public function interpretArticle($articleContent)
    {
        $articleContent = htmlspecialchars($articleContent);

        $articleContent = preg_replace_callback(
            '/@(\w+)/',
            function ($matches) {
                $url = $this->router->generate('profile', ['username' => $matches[1]]);
                return '<a href="'.$url.'">'.$matches[0].'</a>';
            },
            $articleContent
        );

        $articleContent = preg_replace_callback(
            '/#(\w+)/',
            function ($matches) {
                $url = $this->router->generate('hashtag_article', ['hashtag' => $matches[1]]);
                return '<a href="'.$url.'">'.$matches[0].'</a>';
            },
            $articleContent
        );


        return $articleContent;
    }

    public function interpretUsername($username){
        $username =  htmlspecialchars($username);

        $username = preg_replace_callback(
            '/@(\w+)/',
            function ($matches) {
                $url = $this->router->generate('profile', ['username' => $matches[1]]);
                return '<a href="'.$url.'">'.$matches[0].'</a>';
            },
            $username
        );

        return $username;
    }
}