<?php

namespace App\DataFixtures;


use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoders)
    {
        $this->encoder = $encoders;
    }

    public function load(ObjectManager $manager)
    {
        // On configure dans quelles langues nous voulons nos données
        $faker = \Faker\Factory::create('fr_FR');

        // on créé 10 personnes
        $faker = \Faker\Factory::create('fr_FR');

        $user = new User();
        $user->setEmail('grace@yahoo.fr');
        $password = $this->encoder->encodePassword($user, 'admin');
        $user->setPassword($password);
        $user->setUsername('Grace');
        $user->setRoles([
            'ROLE_USER',
            'ROLE_ADMIN',
        ]);
        $user->setImage($faker->imageUrl());
        $manager->persist($user);

        for ($i = 1; $i <= 20; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $password = $this->encoder->encodePassword($user, 'test');
            $user->setPassword($password);
            $user->setUsername($faker->userName);
            $user->setRoles([
                'ROLE_USER',
            ]);
            $user->setImage($faker->imageUrl());
            $manager->persist($user);

            //Pour les articles
            for ($j = 1; $j <= mt_rand(4, 140); $j++) {
                $article = new Article();


                $article->setContent($faker->sentence)
                    ->setCreatedAt($faker->dateTimeBetween('- 6 months'))
                    ->setUser($user)
                ;


                $manager->persist($article);

            }

            $manager->flush();
        }

    }
}
