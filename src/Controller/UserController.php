<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    /**
     * @Route("/users", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/register", name="register", methods={"GET","POST"})
     */
    public function new(Request $request,  UserPasswordEncoderInterface $encoders): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //variable qui permet d'encoder le password en utilisant l'alogorith becrypt defini dans security.yaml
            //qui dit que pour tout quand on tombe sur un User utilisons l'algo bcrypt puis on localise le mot de passe
            // qui est dans User
            $hash = $encoders->encodePassword($user, $user->getPassword());
            //pour modifier le pw et j'ajouter le Hash qui le mot de passe encoder.
            $user->setPassword($hash);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile/{username}", name="profile", methods={"GET"})
     */
    public function show(User $user, ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->findBy([
            'user' => $user
        ]);

        return $this->render('user/show.html.twig', [
            'user' => $user,
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/profile", name="monprofile", methods={"GET"})
     */
    public function user( ArticleRepository $articleRepository): Response
    {
        $user = $this->getUser();
        $articles = $articleRepository->findBy([
            'user' => $user
        ]);

        return $this->render('user/show.html.twig', [
            'user' => $user,
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/follow/{username}", name="follow", methods={"GET"})
     */
    public function follow(User $user, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $actualUser = $this->getUser();

        $actualUser->addFellow($user);
        $entityManager->flush();

        return $this->redirectToRoute('profile', ['username' => $user->getUsername()]);
    }

    /**
     * @Route("/unfollow/{username}", name="unfollow", methods={"GET"})
     */
    public function unfollow(User $user, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $actualUser = $this->getUser();

        $actualUser->removeFellow($user);
        $entityManager->flush();

        return $this->redirectToRoute('profile', ['username' => $user->getUsername()]);
    }
}
